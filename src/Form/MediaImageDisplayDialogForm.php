<?php

declare(strict_types = 1);

namespace Drupal\media_image_widget\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\crop\Entity\Crop;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageEffectInterface;
use Drupal\image_widget_crop\Element\ImageCrop;
use Drupal\media\MediaInterface;
use Drupal\media_image_widget\ViewModeUtility;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Media Image Widget form.
 */
final class MediaImageDisplayDialogForm extends FormBase {

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The view mode utility service.
   *
   * @var \Drupal\media_image_widget\ViewModeUtility
   */
  protected $viewModeUtility;

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\Core\Entity\Display\EntityDisplayInterface
   */
  protected $entity;

  /**
   * Constructs a new DisplaySettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   (optional) The entity display_repository.
   */
  public function __construct(EntityDisplayRepositoryInterface $entity_display_repository, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config, ViewModeUtility $viewModeUtility) {
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config;
    $this->viewModeUtility = $viewModeUtility;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_display.repository'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('media_image_widget.view_mode_utility')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'media_image_widget_display_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntityInterface $entity = NULL, MediaInterface $media = NULL, string $field_name = NULL, int $delta = NULL, string $display_settings = '') : array {
    $meta_data = [
      'entity_type' => $entity->getEntityTypeId(),
      'entity' => $entity->id(),
      'media' => $media->id(),
      'field_name' => $field_name,
      'delta' => $delta,
      'display_settings' => $display_settings,
    ];
    $form_state->set('meta_data', $meta_data);
    $form_state->set('media', $media);
    $ajax_url = Url::fromRoute('media_image_widget.preview_form', $meta_data);
    $ajax_dialog_form_options = [
      // The following too keys are necessary because this is an ajax driven
      // form rendered inside an ajax dialog. See also
      // https://www.drupal.org/project/drupal/issues/2934463
      'url' => $ajax_url,
      'options' => [
        'query' => [
          FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
        ],
      ],
    ];
    $display_settings = json_decode($display_settings, TRUE);

    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#default_value' => $display_settings['view_mode'] ?? '',
      '#empty_option' => $this->t('- Select -'),
      '#options' => $this->viewModeUtility->getAllowedViewModes($entity->getEntityTypeId(), $entity->bundle(), $field_name),
      '#description' => $this->t('Select the view mode you want to use to display the media @media. If none is selected, the default view mode configured in field display settings will be used.', ['@media' => $media->label()]),
      '#ajax' => [
        'wrapper' => 'media-preview-wrapper',
        'callback' => '::viewModeSelectedAjax',
        'method' => 'replace',
      ] + $ajax_dialog_form_options,
    ];

    if ($form_state->getUserInput()['view_mode'] === NULL) {
      $view_mode = $display_settings['view_mode'] ?? '';
    }
    else {
      $view_mode = $form_state->getUserInput()['view_mode'];
    }

    $image_field = $this->viewModeUtility->getImageFieldByMedia($media);
    $form_state->set('media_image_field', $image_field);

    // @todo Should we display the default view mode configured in the widget
    //   settings if no default value and not user input is present?
    $preview = [];
    if ($view_mode) {
      $crop_image_effect = $this->viewModeUtility->getCropEffectByViewMode($media->id(), $view_mode);
      if ($crop_image_effect instanceof ImageEffectInterface) {
        $crop_type = $crop_image_effect->getConfiguration()['data']['crop_type'];
        $form_state->set('crop_type', $crop_type);
        $preview = [
          'crop' => [
            '#type' => 'image_crop',
            '#crop_type_list' => [$crop_type],
            '#file' => $media->get($image_field)->entity,
            '#crop_preview_image_style' => 'crop_thumbnail',
            '#show_default_crop' => TRUE,
            '#show_crop_area' => TRUE,
            '#warn_mupltiple_usages' => FALSE,
            '#process' => [
              [static::class, 'processCropElement'],
            ],
          ]
        ];
      }
      else {
        $preview = [
          'preview' => $this->entityTypeManager->getViewBuilder('media')->view($media, $view_mode)
        ];
      }
    }

    $form['media_preview'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'media-preview-wrapper',
        'class' => ['preview-wrapper'],
      ],
    ] + $preview;

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#ajax' => [
          'callback' => '::submitAjax',
        ] + $ajax_dialog_form_options,
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $media = $form_state->get('media');
    if ($form_state->getTriggeringElement()['#name'] == 'op') {
      $this->messenger()->addStatus($this->t(
        'The display settings for @media have been updated.',
        ['@media' => $media->label() . '(' . $media->id() . ')']
      ));
    }
  }

  /**
   * Ajax callback for the view mode select.
   */
  public function viewModeSelectedAjax($form, FormStateInterface $form_state) : array {
    return $form['media_preview'];
  }

  /**
   * Ajax callback for the submit button to close the modal.
   */
  public function submitAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // Insert the selected display settings into the hidden field on the main
    // form before closing the display settings dialog.
    $meta_data = $form_state->get('meta_data');
    $selector = $this->viewModeUtility->getDisplaySettingsElementSelector(
      $meta_data['media'],
      $meta_data['delta'],
      $meta_data['field_name']
    );

    $view_mode = $form_state->getValue('view_mode');
    $saved_display_settings = [
      'view_mode' => $view_mode,
    ];

    $crop_image_effect = $this->viewModeUtility->getCropEffectByViewMode($meta_data['media'], $view_mode);
    if ($crop_image_effect instanceof ImageEffectInterface) {
      $crop_data = $form_state->getValue('crop')['crop_wrapper'][$form_state->get('crop_type')]['crop_container']['values'];
      // CropEffect::applyEffects() is using crop->anchor(), which is equal to the
      // top left coordinates of the crop in the image. But the x/y coordinates we
      // get submitted here represent the center of the crop. So for the applying
      // the crop and also for displaying the correct default crop position on the
      // edit form, we need to save the top left (anchor), not the center.
      $crop_data['x'] = $crop_data['x'] + ($crop_data['width'] / 2);
      $crop_data['y'] = $crop_data['y'] + ($crop_data['height'] / 2);

      // @todo Delete a previous crop if it existed for the media, delta,
      //   fieldname and image uri.
      // @todo Should we load the existing crop according to value of uri field
      //   below? This would require some kind of hash based on the crop data to
      //   be appended to the uri to invalidate images cached on CDNs.
      $crop = $this->entityTypeManager->getStorage('crop')->create([
          'type' => $form_state->get('crop_type'),
          'entity_id' => $meta_data['entity'],
          'entity_type' => $meta_data['entity_type'],
          'uri' => implode('__', [
            $form_state->get('media')->get($form_state->get('media_image_field'))->entity->getFileUri(),
            $meta_data['media'],
            $meta_data['delta'],
            $meta_data['field_name'],
          ]),
        ] + $crop_data);
      $crop->save();

      $saved_display_settings += [
        'crop_data' => [
          'crop' => $crop->id(),
          'image_style' => $this->viewModeUtility->getImageStyleByViewMode($meta_data['media'], $view_mode),
        ]
      ];
    }

    $response->addCommand(new InvokeCommand(
        '.' . $selector,
        'val',
        [json_encode($saved_display_settings)]
      ));

    if (!$form_state->getErrors()) {
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

  /**
   * Render API callback: Expands the image_crop element type.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   form actions container.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processCropElement(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $element = ImageCrop::processCrop($element, $form_state, $complete_form);

    /** @var \Drupal\file\Entity\File $file */
    $file = $element['#file'];
    /** @var \Drupal\Core\Image\Image $image */
    $image = \Drupal::service('image.factory')->get($file->getFileUri());
    if (!$image->isValid()) {
      return $element;
    }

    $crop_type_list = $element['#crop_type_list'];
    /** @var \Drupal\Core\Config\Entity\ConfigEntityStorage $crop_type_storage */
    $crop_type_storage = \Drupal::entityTypeManager()
      ->getStorage('crop_type');

    /** @var \Drupal\crop\Entity\CropType[] $crop_types */
    if ($crop_types = $crop_type_storage->loadMultiple($crop_type_list)) {
      foreach ($crop_types as $type => $crop_type) {

        $properties = [];
        $form_state_values = $form_state->getValue($element['#parents']);
        // Check if form state has values.
        if (ImageCrop::hasCropValues($element, $type, $form_state)) {
          $form_state_properties = $form_state_values['crop_wrapper'][$type]['crop_container']['values'];
          // If crop is applied by the form state we keep it that way.
          if ($form_state_properties['crop_applied'] == '1') {
            $element['crop_wrapper'][$type]['crop_container']['values']['crop_applied']['#default_value'] = 1;
            $edit = TRUE;
          }
          $properties = $form_state_properties;
        }

        $meta_data = $form_state->get('meta_data');
        $crop_load_uri = implode('__', [
          $form_state->get('media')->get($form_state->get('media_image_field'))->entity->getFileUri(),
          $meta_data['media'],
          $meta_data['delta'],
          $meta_data['field_name'],
        ]);
        $crop = Crop::findCrop($crop_load_uri, $type);
        if ($crop instanceof Crop) {
          $edit = TRUE;
          /** @var \Drupal\image_widget_crop\ImageWidgetCropInterface $iwc_manager */
          $iwc_manager = \Drupal::service('image_widget_crop.manager');
          $original_properties = $iwc_manager->getCropProperties($crop);

          // If form state values have the same values that were saved or if
          // form state has no values yet and there are saved values then we
          // use the saved values.
          $properties = $original_properties == $properties || empty($properties) ? $original_properties : $properties;
          $element['crop_wrapper'][$type]['crop_container']['values']['crop_applied']['#default_value'] = 1;
          // If the user edits an entity and while adding new images resets an
          // saved crop we keep it reset.
          if (isset($properties['crop_applied']) && $properties['crop_applied'] == '0') {
            $element['crop_wrapper'][$type]['crop_container']['values']['crop_applied']['#default_value'] = 0;
          }
        }
        ImageCrop::getCropFormElement($element, 'crop_container', $properties, $edit, $type);
      }
    }

    return $element;
  }

}
