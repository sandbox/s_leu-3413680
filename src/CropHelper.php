<?php

declare(strict_types=1);

namespace Drupal\media_image_widget;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\crop\CropInterface;
use Drupal\media\MediaInterface;

/**
 * Provides functionality to facilitate cropping of images in media image fields.
 */
final class CropHelper {

  const CROP_ID_QUERY_PARAMETER = 'miw_crop_id';

  public function appendCropQuery(string &$uri, int $crop_id): void {
    // Ensure the crop can be accessed in media_image_widget_file_alter() to
    // create crop specific image URIs.
    $uri .= \str_contains('?', $uri) ? '?' : '&';
    $uri = $uri . self::CROP_ID_QUERY_PARAMETER . '=' . $crop_id;
  }

  public function cropDerivateUri(string $uri, CropInterface $crop): string {
    $uri_parts = pathinfo($uri);
    $crop_aware_filename = $uri_parts['filename'] . $this->cropUriFilenameSuffix($crop);
    return \str_replace($uri_parts['filename'], $crop_aware_filename, $uri);
  }

  public function cropUriFilenameSuffix(CropInterface $crop): string {
    return '__crop__' . $crop->id();
  }

}
