<?php

namespace Drupal\media_image_widget\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $route = $collection->get('image.style_public');
    if ($route) {
      $existing_defaults = $route->getDefaults();
      $existing_defaults['_controller'] = '\Drupal\media_image_widget\Controller\CropAwareImageStyleDownloadController::deliver';
      $route->setDefaults($existing_defaults);
    }
  }

}
