<?php

namespace Drupal\media_image_widget\Plugin\ImageEffect;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\crop\CropInterface;
use Drupal\crop\Plugin\ImageEffect\CropEffect;
use Drupal\media_image_widget\CropHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Crops an image resource.
 *
 * @ImageEffect(
 *   id = "media_image_widget_crop",
 *   label = @Translation("Media Image Widget Crop"),
 *   description = @Translation("Applies a via Media Image Widget manually set crop to the image.")
 * )
 */
class MediaImagWidgetCropEffect extends CropEffect {

  private readonly RequestStack $requestStack;
  private readonly EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->requestStack = $container->get('request_stack');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCrop(ImageInterface $image) {
    // @todo check if we should rely solely on filename containing crop id instead.
    if (!$this->requestStack->getCurrentRequest()->query->has(CropHelper::CROP_ID_QUERY_PARAMETER)) {
      return FALSE;
    }
    $crop_id = $this->requestStack->getCurrentRequest()->query->get(CropHelper::CROP_ID_QUERY_PARAMETER);
    $crop = $this->entityTypeManager->getStorage('crop')->load($crop_id);
    if ($crop instanceof CropInterface) {
      $this->crop = $crop;
      return $this->crop;
    }
    return FALSE;
  }

}
