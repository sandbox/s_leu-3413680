<?php declare(strict_types = 1);

namespace Drupal\media_image_widget\Plugin\Field\FieldType;

use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldTypeCategoryManagerInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;

#[FieldType(
  id: "media_image_field",
  label: new TranslatableMarkup("Media Image"),
  description: new TranslatableMarkup("Stores an image media reference along with metadata that controls the image display."),
  category: FieldTypeCategoryManagerInterface::FALLBACK_CATEGORY,
  default_widget: "media_image_widget",
  default_formatter: "media_image_formatter",
  list_class: EntityReferenceFieldItemList::class,
)]
final class MediaImageFieldItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'media',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element['target_type'] = [
      '#type' => 'value',
      '#value' => 'media',
    ];
    // @todo should the media bundle be selected here in a select listing only
    //   media bundles with image fields?
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::fieldSettingsForm($form, $form_state);
    $allowed_bundles = [];
    /** @var \Drupal\media\Entity\MediaType[] $media_types */
    $media_types = \Drupal::entityTypeManager()->getStorage('media_type')->loadMultiple();
    foreach ($media_types as $type) {
      $source_field = $type->getSource()->getSourceFieldDefinition($type);
      if ($source_field->getType() == 'image') {
        $allowed_bundles[$type->id()] = $type->label();
      }
    }
    $form['handler']['handler_settings']['target_bundles']['#options'] = $allowed_bundles;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $target_id_definition = DataReferenceTargetDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('@label ID', ['@label' => 'Media']))
      ->setSetting('unsigned', TRUE)
      ->setRequired(TRUE);
    $properties['target_id'] = $target_id_definition;

    $display_options_definition = DataReferenceTargetDefinition::create('string')
      ->setLabel('Display settings')
      ->setDescription('Stores metadata controlling the display of a referenced media in JSON format.');
    $properties['display_settings'] = $display_options_definition;

    $properties['entity'] = DataReferenceDefinition::create('entity')
      ->setLabel('Media')
      ->setDescription(new TranslatableMarkup('The referenced media entity'))
      // The entity object is computed out of the entity ID.
      ->setComputed(TRUE)
      ->setReadOnly(FALSE)
      ->setTargetDefinition(EntityDataDefinition::create('media'))
      // We can add a constraint for the target entity type. The list of
      // referenceable bundles is a field setting, so the corresponding
      // constraint is added dynamically in ::getConstraints().
      ->addConstraint('EntityType', 'media');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {

    $columns = [
      'target_id' => [
        'description' => 'The ID of the target entity.',
        'type' => 'int',
        'unsigned' => TRUE,
      ],
      'display_settings' => [
        'type' => 'varchar',
        'not null' => FALSE,
        'description' => 'The display options applied when rendering the media.',
        'length' => 255,
      ],
    ];

    $schema = [
      'columns' => $columns,
      'indexes' => [
        'target_id' => ['target_id'],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function storageSettingsSummary(FieldStorageDefinitionInterface $storage_definition): array {
    return [];
  }

}
