<?php declare(strict_types = 1);

namespace Drupal\media_image_widget\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\media_image_widget\Form\MediaImageDisplayDialogForm;
use Drupal\media_image_widget\ViewModeUtility;
use Drupal\media_library\Plugin\Field\FieldWidget\MediaLibraryWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

#[FieldWidget(
  id: 'media_image_widget',
  label: new TranslatableMarkup('Media Image Widget'),
  description: new TranslatableMarkup('Allows you to select media image items from the media library and customize their display.'),
  field_types: ['media_image_field'],
  multiple_values: TRUE,
)]
final class MediaImageWidget extends MediaLibraryWidget {

  protected ViewModeUtility $viewModeUtility;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->viewModeUtility = $container->get('media_image_widget.view_mode_utility');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'allowed_view_modes' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function getAllowedMediaTypeIdsSorted() {
    $types = parent::getAllowedMediaTypeIdsSorted();
    // @todo filter the types to ensure only types containing images can be used.
    return $types;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $referenced_entities = $items->referencedEntities();
    if (!$referenced_entities) {
      return $element;
    }
    $field_name = $this->fieldDefinition->getName();
    $parents = $form['#parents'];
    $id_suffix = $parents ? '-' . implode('-', $parents) : '';
    $wrapper_id = $field_name . '-media-library-wrapper' . $id_suffix;
    $limit_validation_errors = [array_merge($parents, [$field_name])];

    foreach ($referenced_entities as $delta => $media_item) {
      $element['selection'][$delta]['customize_button'] = [
        '#type' => 'submit',
        '#name' => $field_name . '-' . $delta . '-media-library-customize-button' . $id_suffix,
        '#value' => $this->t('Customize display'),
        '#media' => $media_item,
        '#attributes' => [
          'aria-label' => $media_item->access('view label') ? $this->t('Customize @label display', ['@label' => $media_item->label()]) : $this->t('Customize display'),
        ],
        '#ajax' => [
          'callback' => [static::class, 'openDisplaySettings'],
          'wrapper' => $wrapper_id,
          'progress' => [
            'type' => 'throbber',
            'message' => $media_item->access('view label') ? $this->t('Customizing @label. display', ['@label' => $media_item->label()]) : $this->t('Customizing media display.'),
          ],
        ],
        // Prevent errors in other widgets from preventing removal.
        '#limit_validation_errors' => $limit_validation_errors,
      ];

      $element['selection'][$delta]['display_settings'] = [
        '#type' => 'textfield',
        '#default_value' => $form_state->getFormObject()->getEntity()->get($field_name)->get($delta)->display_settings,
        '#attributes' => [
          'class' => [
            $this->viewModeUtility->getDisplaySettingsElementSelector(
              $media_item->id(),
              $delta,
              $field_name
            ),
          ],
        ],
      ];
    }

    return $element;
  }

  /**
   * AJAX callback to open display settings modal.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An AJAX response to open the media library.
   */
  public static function openDisplaySettings(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#parents'];
    unset($parents[count($parents) - 1]);
    $media = $triggering_element['#media'];

    $dialog_content = \Drupal::service('form_builder')->getForm(
      MediaImageDisplayDialogForm::class,
      $form_state->getFormObject()->getEntity(),
      $media,
      $parents[0],
      (int) $parents[2],
      NestedArray::getValue($form_state->getUserInput(), array_merge($parents, ['display_settings'])),
    );
    $dialog_options = [
      'dialogClass' => 'media-image-widget-modal',
      'title' => t('Customize display settings for @media', ['@media' => $media->label()]),
      'minHeight' => '600',
      'minWidth' => '1200',
    ];
    return (new AjaxResponse())
      ->addCommand(new OpenModalDialogCommand($dialog_options['title'], $dialog_content, $dialog_options));
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['allowed_view_modes'] = [
      '#title' => $this->t('Limit Allowed View Modes'),
      '#description' => $this->t('Limits the view modes that are available for selection on the customize display form. The list here is already limited to view modes that are configured on the entity bundle this field is attached to.<strong>If none are selected, all view modes will be allowed.</strong>'),
      '#type' => 'checkboxes',
      '#options' => $this->getEnabledViewModes(),
      '#weight' => -99,
      '#default_value' => $this->settings['allowed_view_modes'] ?? [],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $enabled_view_modes = $this->getEnabledViewModes();
    $view_modes = !$this->settings['allowed_view_modes']
      ? $enabled_view_modes
      : array_intersect_key($enabled_view_modes, array_flip($this->settings['allowed_view_modes']));
    $summary[] = $this->t('Allowed view modes: @view_modes', ['@view_modes' => implode(', ', $view_modes)]);
    return $summary;
  }

  /**
   * Gets all view modes the field is configured on target entity and bundle.
   */
  private function getEnabledViewModes() : array {
    return $this->viewModeUtility->getEnabledViewModes(
      $this->fieldDefinition->getTargetEntityTypeId(),
      $this->fieldDefinition->getTargetBundle(),
      $this->fieldDefinition->getName()
    );
  }

}
