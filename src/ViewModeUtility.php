<?php

declare(strict_types = 1);

namespace Drupal\media_image_widget;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\image\ImageEffectInterface;
use Drupal\image\ImageStyleInterface;
use Drupal\media\MediaInterface;

/**
 * Helper service providing view mode related utility functions.
 */
final class ViewModeUtility {

  /**
   * Constructs a ViewModeUtility object.
   */
  public function __construct(
    private readonly EntityDisplayRepositoryInterface $entityDisplayRepository,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly EntityFieldManagerInterface $entityFieldManager,
  ) {}

  /**
   * Gets view modes the given field is enabled on the entity type and bundle.
   */
  public function getEnabledViewModes(string $entity_type_id, string $bundle, string $field_name) : array {
    $parent_entity_view_modes = $this->entityDisplayRepository->getViewModeOptionsByBundle($entity_type_id, $bundle);
    $config_prefix = 'core.entity_view_display.' . $entity_type_id . '.' . $bundle . '.';
    $relevant_media_view_modes = [];
    $media_view_modes = $this->entityDisplayRepository->getViewModeOptions('media');

    foreach (array_keys($parent_entity_view_modes) as $view_mode_id) {
      $entity_view_config = $this->configFactory->get($config_prefix . $view_mode_id);

      // We only want to allow previews of the media in the view modes the host
      // entity was actually configured to display the media.
      $entity_view_content = $entity_view_config->get('content');
      if ($entity_view_content && array_key_exists($field_name, $entity_view_content) && !empty($entity_view_content[$field_name]['settings']['view_mode'])) {
        $configured_view_mode = $entity_view_content[$field_name]['settings']['view_mode'];
        $relevant_media_view_modes[$configured_view_mode] = $media_view_modes[$configured_view_mode] ?? $configured_view_mode;
      }
    }
    return $relevant_media_view_modes;
  }

  /**
   * Gets view modes that will be available on the display settings modal form.
   *
   * Only view modes that are white-listed ("allowed") in the field's display
   * settings of the given entity type and bundle are returned.
   */
  public function getAllowedViewModes(string $entity_type_id, string $bundle, string $field_name, string $form_mode = 'default') : array {
    $enabled_view_modes = $this->getEnabledViewModes($entity_type_id, $bundle, $field_name);
    $config_name = 'core.entity_form_display.' . $entity_type_id . '.' . $bundle . '.' . $form_mode;
    $entity_form_display = $this->configFactory->get($config_name);

    if (empty($entity_form_display->get('content')[$field_name])) {
      return $enabled_view_modes;
    }

    $allowed_view_modes = $entity_form_display->get('content')[$field_name]['settings']['allowed_view_modes'];
    $allowed_view_modes = array_filter($allowed_view_modes) ?? [];
    return !$allowed_view_modes
      ? $enabled_view_modes
      : array_intersect_key($enabled_view_modes, array_flip($allowed_view_modes));
  }

  /**
   * Gets a unique string to identify the form element holding display settings.
   */
  public function getDisplaySettingsElementSelector(int|string $media_id, int $delta, string $field_name) {
    return implode('--', [
      'display-settings-value',
      $field_name,
      $delta,
      $media_id,
    ]);
  }

  public function getCropEffectByViewMode(int|string $media_id, string $view_mode) : ?ImageEffectInterface {
    $image_style_id = $this->getImageStyleByViewMode($media_id, $view_mode);

    if (\is_null($image_style_id)) {
      return NULL;
    }

    /** @var \Drupal\image\Entity\ImageStyle $image_style */
    $image_style = $this->entityTypeManager->getStorage('image_style')->load($image_style_id);
    foreach ($image_style->getEffects() as $image_effect) {
      if ($image_effect->getPluginId() == 'media_image_widget_crop') {
        return $image_effect;
      }
    }
    return NULL;
  }

  public function getImageStyleByViewMode(int|string $media_id, string $view_mode) : ?string {
    /** @var \Drupal\media\MediaInterface $media */
    $media = $this->entityTypeManager->getStorage('media')->load($media_id);
    $config_name = 'core.entity_view_display.media.' . $media->bundle() . '.' . $view_mode;
    $display_config = $this->configFactory->get($config_name);
    $media_image_field = $this->getImageFieldByMedia($media);

    // If the image field is not rendered at all or not rendered using an image
    // style, there is no crop effect associated with it.
    return !empty($display_config->get('content')[$media_image_field]['settings']['image_style'])
      ? $display_config->get('content')[$media_image_field]['settings']['image_style']
      : NULL;
  }

  public function getImageFieldByMedia(MediaInterface $media): ?string {
    $image_fields = $this->entityFieldManager->getFieldMapByFieldType('image');
    if (\array_key_exists('media', $image_fields)) {
      $media_image_fields = $image_fields['media'];

      // Ignore the Media entity's thumbnail basefield here as it's irrelevant.
      unset($media_image_fields['thumbnail']);
      foreach ($media_image_fields as $field_name => $field_info) {
        if (\in_array($media->bundle(), $field_info['bundles'], TRUE)) {
          // The media widget supports only one image field per media type,
          // which is the field that gets discovered first.
          return $field_name;
        }
      }
    }
    return NULL;
  }

}
